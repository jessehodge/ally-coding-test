import click
import requests

@click.group()
def cli():
    pass

@cli.command()
def loc():
    print(requests.get('http://api.open-notify.org/iss-now.json').json())
 
@cli.command()
@click.argument('lat')
@click.argument('long')
def passing(lat, long):
    response = requests.get(f'http://api.open-notify.org/iss-pass.json?lat={lat}&lon={long}')
    time = response.json()['request']['datetime']
    duration = sum([obj['duration'] for obj in response.json()['response']])
    print(f'The ISS will be overhead {lat, long} at {time} for {duration}')

@cli.command()
def people():
    response = requests.get('http://api.open-notify.org/astros.json')
    number = response.json()['number']
    names = ', '.join([obj['name'] for obj in response.json()['people']])
    craft = response.json()['people'][0]['craft']
    print(f'There are {number} people on the {craft}. They are {names}.')

if __name__ == '__main__':
    cli()
